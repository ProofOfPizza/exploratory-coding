"use strict";
exports.__esModule = true;
var tree_1 = require("./tree");
var tree = {
    value: 1,
    left: {
        value: 21,
        left: { value: 31 },
        right: { value: 32, left: { value: 41 }, right: { value: 42 } }
    },
    right: { value: 22 }
};
var f = function (value) {
    console.log("hi you have reached: ".concat(value));
    return 2 * value;
};
// const mapResult = treeMap(tree, f);
// console.log(mapResult);
var tmt = (0, tree_1.treeMapTree)(tree, f);
console.log(JSON.stringify(tmt, null, 2));
var treeString = {
    value: "hallo",
    left: {
        value: "meneer",
        left: { value: "de" },
        right: {
            value: "uil",
            left: { value: "Hoe" },
            right: { value: "maakt U" }
        }
    },
    right: { value: "het?" }
};
var strResult = (0, tree_1.treeMapTree)(treeString, function (x) { return x.toUpperCase(); });
var str2Result = (0, tree_1.treeMapTree)(treeString, function (x) {
    return x.length > 3 ? true : "ok";
});
console.log(JSON.stringify(strResult, null, 2));
console.log(JSON.stringify(str2Result, null, 2));
