export interface TreeNode<T extends any> extends Leaf<T> {
  right: TreeNode<T> | Leaf<T>;
  left: TreeNode<T> | Leaf<T>;
}

export interface Leaf<T extends any> {
  value: T;
}

export const treeMap = (tree: TreeNode<any>, f: (x: any) => any): any[] => {
  const result: number[] = [];
  result.push(f(tree.value));
  if (tree.left && tree.right) {
    result.push(...treeMap(tree.left as TreeNode<any>, f));
    result.push(...treeMap(tree.right as TreeNode<any>, f));
  }
  return result;
};

export const treeMapTree = (
  tree: TreeNode<any>,
  f: (x: any) => any
): TreeNode<any> | Leaf<any> => {
  const resultTree = { value: f(tree.value) } as TreeNode<any> | Leaf<any>;
  if (tree.left && tree.right) {
    (resultTree as TreeNode<any>).left = treeMapTree(
      tree.left as TreeNode<any>,
      f
    );
    (resultTree as TreeNode<any>).right = treeMapTree(
      tree.right as TreeNode<any>,
      f
    );
  }
  return resultTree;
};

