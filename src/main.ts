import { TreeNode, treeMap, treeMapTree } from "./tree";
const tree: TreeNode<number> = {
  value: 1,
  left: {
    value: 21,
    left: { value: 31 },
    right: { value: 32, left: { value: 41 }, right: { value: 42 } },
  },
  right: { value: 22 },
};

const f = (value: number) => {
  console.log(`hi you have reached: ${value}`);
  return 2 * value;
};

// const mapResult = treeMap(tree, f);

// console.log(mapResult);

const tmt = treeMapTree(tree, f);
console.log(JSON.stringify(tmt, null, 2));

const treeString: TreeNode<string> = {
  value: "hallo",
  left: {
    value: "meneer",
    left: { value: "de" },
    right: {
      value: "uil",
      left: { value: "Hoe" },
      right: { value: "maakt U" },
    },
  },
  right: { value: "het?" },
};

const strResult = treeMapTree(treeString, (x: string) => x.toUpperCase());
const str2Result = treeMapTree(treeString, (x: string) =>
  x.length > 3 ? true : "ok"
);
console.log(JSON.stringify(strResult, null, 2));
console.log(JSON.stringify(str2Result, null, 2));

