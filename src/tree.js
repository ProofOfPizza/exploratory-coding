"use strict";
exports.__esModule = true;
exports.treeMapTree = exports.treeMap = void 0;
var treeMap = function (tree, f) {
    var result = [];
    result.push(f(tree.value));
    if (tree.left && tree.right) {
        result.push.apply(result, (0, exports.treeMap)(tree.left, f));
        result.push.apply(result, (0, exports.treeMap)(tree.right, f));
    }
    return result;
};
exports.treeMap = treeMap;
var treeMapTree = function (tree, f) {
    var resultTree = { value: f(tree.value) };
    if (tree.left && tree.right) {
        resultTree.left = (0, exports.treeMapTree)(tree.left, f);
        resultTree.right = (0, exports.treeMapTree)(tree.right, f);
    }
    return resultTree;
};
exports.treeMapTree = treeMapTree;
