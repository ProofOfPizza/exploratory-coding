{ pkgs ? import <nixpkgs> { } }:
let unstable = import <nixos-unstable> { overlays = [ (import ./cypress-overlay.nix) ]; };

    cypress = unstable.callPackage ./cypress.nix {};
in
pkgs.mkShell {
  buildInputs = [
    unstable.nodejs-16_x
    unstable.awscli2
    unstable.python27
    unstable.yarn
    unstable.sass
    unstable.terraform
  ];
  shellHook = ''
    export CYPRESS_INSTALL_BINARY=0
    export CYPRESS_RUN_BINARY=${unstable.cypress}/bin/Cypress
  '';
}

